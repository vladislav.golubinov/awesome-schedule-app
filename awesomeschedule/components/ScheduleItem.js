
import React, { Component } from 'react'
import { Dimensions, StyleSheet, View, Animated } from 'react-native'

import Block from './Block';
import Text from './Text';
import Card from './Card';
import { theme } from '../constants';

const { width } = Dimensions.get('window');

export default class ScheduleItem extends Component {
    render() {
        return (
            <Card shadow style={styles.item}>
                <Block row>
                    <Block left>
                        <Text bold h2>{this.props.subject}</Text>
                    </Block>
                    <Block right>
                        <Text>{this.props.type}</Text>
                    </Block>
                </Block>
                <Block row>
                    <Block>
                        <Text>{this.props.time}</Text>
                    </Block>
                    <Block>
                        <Text>a.{this.props.auditorium}</Text>
                    </Block>
                </Block>
            </Card>
        )
    }
}

export const styles = StyleSheet.create({
    item: {
        maxWidth: width - 20,
        maxHeight: 100,
    }
})