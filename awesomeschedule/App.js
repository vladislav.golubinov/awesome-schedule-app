import React from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';

import { AppLoading } from 'expo'
import { Asset } from 'expo-asset'

import Navigation from './navigation';
import Block from './components/Block'


const images = [
	require('./assets/icon.png'),
	require('./assets/splash.png'),
	require('./assets/icons/back.png'),
	require('./assets/icons/plants.png'),
	require('./assets/icons/seeds.png'),
	require('./assets/icons/flowers.png'),
	require('./assets/icons/sprayers.png'),
	require('./assets/icons/pots.png'),
	require('./assets/icons/fertilizers.png'),
	require('./assets/images/illustration_1.png'),
	require('./assets/images/illustration_2.png'),
	require('./assets/images/illustration_3.png'),
	require('./assets/images/avatar.png'),
];

export default class App extends React.Component {
	state = {
		isLoadingComplete: false,
	}

	handleResourcesAsync = async () => {
		const cacheImages = images.map(image => {
			return Asset.fromModule(image).downloadAsync();
		});
 
		return Promise.all(cacheImages);
	}

	render() {
		if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
			return (
				<AppLoading
					startAsync={this.handleResourcesAsync}
					onError={error => console.warn(error)}
					onFinish={() => this.setState({ isLoadingComplete: true })}
				/>
			)
		}

		return (
			<Block white>
				<StatusBar barStyle="dark-content" />
				<Navigation />
			</Block>
		)
	}
}

const styles = StyleSheet.create({});