import React, { Component } from 'react';
import { Dimensions, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'

import { Button, Block, Text, Card, ScheduleItem } from '../components';
import { theme, env } from '../constants';

const { width } = Dimensions.get('window');

export default class Browse extends Component {
    static navigationOptions = {
        header: null,
    }


    render() {
        const { navigation } = this.props;

        return (
            <Block>
                <Block margin={[theme.sizes.padding * 2, theme.sizes.padding]}>
                    <Text h1>
                        Today's your
                    </Text>
                    <Text h1 bold>
                        Awesome Schedule
                    </Text>
                </Block>
                <Block middle>
                    <ScheduleItem
                        subject='Computer science'
                        type='Work'
                        time='12:00 - 13:35'
                        auditorium='6430'
                        // teacher_fullname='Kirill Russo'
                    />
                </Block>
            </Block>
            // <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            //     <Text>{this.props.navigation.state.params.accesskey}</Text>
            // </View>
        );
    }
}

const styles = StyleSheet.create({
    item: {
        maxWidth: width - 20,
        maxHeight: 100,
    }
})